Workshop agile testing
======================

This project contains Cucumber and FitNesse tests, both for backend and frontend testing.

Get this stuff up and running
-----------------------------

The workshop application can be started via Maven.

To start the server:

	$ mvn jetty:run

To execute the Cucumber tests

	$ mvn test

To launch FitNesse:

	$ mvn -Pfitnesse test

In order to run the web tests, you'll need to have the jetty server running as described above.

