@exercise1


Feature: Moving the token over the board

  As a player
  I want to move to new positions on the board
  So that I can get rich and win the game

  Scenario Outline: Moving a pawn after a single throw
    Given players token is at starting position <startingPosition>
    When player throws <die1> and <die2>
    Then players token moves to a <newPosition>

  Examples:
    | startingPosition | die1 | die2 | newPosition |
    | 1                | 1    | 2    | 4           |
    | 2                | 3    | 4    | 9           |
    | 39               | 2    | 2    | 3           |


  Scenario Outline: Roll again on same eyes
    Given player his turn
    When player throws <die1> and <die2>
    Then player gets another turn <anotherTurn>

  Examples:
    | die1 | die2 | anotherTurn |
    | 1    | 1    | "Yes"       |
    | 5    | 3    | "No"        |


  Scenario Outline: Going to Jail after rolling same eyes 3 times
    Given a player his turn <turn>
    When player throws <die1> and <die2>
    Then player goes to jail <goesToJail?>

  Examples:
    | turn | die1 | die2 | goesToJail? |
    | 1    | 3    | 3    | "No"        |
    | 2    | 2    | 2    | "No"        |
    | 3    | 4    | 4    | "Yes"       |

