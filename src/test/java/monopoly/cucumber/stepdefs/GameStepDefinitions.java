package monopoly.cucumber.stepdefs;


import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import monopoly.helpers.GameHelper;

import static monopoly.helpers.HelperFunctions.convertBooleanToYesOrNo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class GameStepDefinitions {

    private GameHelper game;

    public GameStepDefinitions(){
        game = new GameHelper();
    }


    @Before
    public void setupGame() {
        game.setupGame();
    }

    @Given("^players token is at starting position (\\d+)$")
    public void startPlayerAtPosition(int position){
        game.getPlayer().setCurrentPosition(position);
    }

    @When("^player throws (\\d+) and (\\d+)$")
    public void player_throws_doubles_given_value_of_dice_and_value_of_dice(int die1, int die2) throws Throwable {
        game.setDice(die1, die2);
    }

    @Then("^players token moves to a (\\d+)$")
    public void player_s_token_moves_to_a(int location) throws Throwable {
        game.doPlayAction();
        assertThat(game.getNewPosition(), is(location));
    }

    @Given("^player his turn$")
    public void player_his_turn() throws Throwable {
    }


    @Given("^a player his turn (\\d+)$")
    public void a_player_his_turn(int turn) throws Throwable {
        game.jumpToTurn(turn);
    }


    @Then("^player gets another turn \"([^\"]*)\"$")
    public void player_gets_another_turn(String expectedResult) throws Throwable {
        game.doPlayAction();
        assertEquals(expectedResult, convertBooleanToYesOrNo(game.getPlayer().isRollAllowed()));
    }


    @Then("^player goes to jail \"([^\"]*)\"$")
    public void player_goes_to_jail(String expectedResult) throws Throwable {
        game.doPlayAction();
        assertEquals(expectedResult, convertBooleanToYesOrNo(game.getPlayer().isJailed()));
    }



}
