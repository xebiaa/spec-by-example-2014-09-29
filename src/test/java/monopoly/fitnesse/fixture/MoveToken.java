package monopoly.fitnesse.fixture;

import monopoly.helpers.GameHelper;

import static monopoly.helpers.HelperFunctions.convertBooleanToYesOrNo;

/**
 * FitNesse fixture
 */
public class MoveToken {

    private GameHelper game;

    private int die1, die2;

    /**
     * Clear data for each action.
     */
    public void reset() {
        game = new GameHelper();
        game.setupGame();
    }

    public void setStartingPosition(int pos) {
        game.getPlayer().setCurrentPosition(pos);
    }

    public void setTurn(int turn) {
        game.jumpToTurn(turn);
    }

    public void setDie1(int die1) {
        this.die1 = die1;
    }

    public void setDie2(int die2) {
        this.die2 = die2;
    }

    /**
     *  Called after all properties are set, before properties are read.
     */
    public void execute() {
        game.setDice(die1, die2);
        game.doPlayAction();
    }

    public int newPosition() {
        return game.getNewPosition();
    }

    public String anotherTurn() {
        return convertBooleanToYesOrNo(game.getPlayer().isRollAllowed());
    }

    public String goesToJail() {
        return convertBooleanToYesOrNo(game.getPlayer().isJailed());
    }
}
