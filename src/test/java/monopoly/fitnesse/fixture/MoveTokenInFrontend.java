package monopoly.fitnesse.fixture;

import monopoly.pageobjects.AddUserPage;
import monopoly.pageobjects.PlayGamePage;
import nl.javadude.monopoly.domain.Dice;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static monopoly.helpers.HelperFunctions.convertBooleanToYesOrNo;

/**
 * FitNesse fixture.
 * @See http://fitnesse.org/FitNesse.UserGuide.WritingAcceptanceTests.SliM.DecisionTable
 */
public class MoveTokenInFrontend {

    private WebDriver webDriver;

    private int die1, die2;
    private PlayGamePage playGamePage;

    /**
     * Called before the decision table is executed.
     */
    public void beginTable() {
        webDriver = new FirefoxDriver();
    }

    /**
     * Called after the decision table is executed.
     */
    public void endTable() {
        webDriver.close();
    }

    /**
     * Called before a row is executed.
     */
    public void reset() {
        webDriver.manage().deleteAllCookies();
        webDriver.navigate().refresh();
        AddUserPage addUserPage = new AddUserPage(webDriver);
        addUserPage.get();
        playGamePage = addUserPage.addUser("kishen").addUser("arjan").startGame();
    }

    public void setTurn(int turn) {
        for (int i = 1; i < turn; i++) {
            playGamePage.rollDice(1, 1);
        }
    }

    public void setDie1(int die1) {
        this.die1 = die1;
    }

    public void setDie2(int die2) {
        this.die2 = die2;
    }

    /**
     *  Called after all properties are set, before properties are read.
     */
    public void execute() {
        playGamePage.rollDice(die1, die2);
    }

    public int newPosition() {
        return playGamePage.getCurrentPlayerPosition();
    }

    public String anotherTurn() {
        return convertBooleanToYesOrNo(playGamePage.isRollAllowed());
    }

    public String goesToJail() throws InterruptedException {
        return convertBooleanToYesOrNo(playGamePage.isJailed());
    }

}
